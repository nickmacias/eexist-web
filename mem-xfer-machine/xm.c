#include <stdio.h>

int mem[65536]; // main mem!
#define PC mem[65535] // PC is actually stored at end of mem
#define DEBUG (mem[65534]) // set to 1 to turn on debugging

main(int argc, char **argv)
{
  FILE *fp;
  char buffer[120];
  int addr=0,data,t1,t2,from,to,status;

// load program into memory
  if (argc==2) fp=fopen(argv[1],"r"); else fp=stdin;
  while (NULL != fgets(buffer,120,fp)){
    if (2==(status=sscanf(buffer,"%d:%d",&t1,&t2))){ // address:data
      addr=t1;data=t2;
    } else data=t1; // just data
    if (status>=1) mem[addr++]=data;
  }

// main processing loop...
  while (1){
    if (DEBUG) printf("%d:%d[%d]=>%d\n",PC,mem[PC],memRead(mem[PC]),mem[PC+1]);
    from=mem[PC++];PC&0xffff;
    to=mem[PC++];PC=PC&0xffff;
    memWrite(to,memRead(from));
  }
}

// memory-mapped-hardware simulation

int alufunc=0; // 1=+,2=-,3=*,4=/
int aluA=0,aluB=0;
int compfunc=0; //1=> 2==
int compA=0,compB=0;

memWrite(int loc, int data)
{
  switch (loc){
// 1000=DISPLAY
    case 1000:display(data);break;

// 1010=ALU
    case 1010:alufunc=data;break;
    case 1011:aluA=data;break;
    case 1012:aluB=data;break;

//1020=COMPARATOR
    case 1020:compfunc=data;break;
    case 1021:compA=data;break;
    case 1022:compB=data;break;

// If not HW, then just write to memory
    default:mem[loc]=data;
  }
}

memRead(int loc)
{
  switch(loc){
// ALU
    case 1013: // ALU
      return(aluvalue());

// COMPARATOR
    case 1023: // comparator
      return(compvalue());

// Not HW: just read from memory
    default:return(mem[loc]);
  }
}

// support code

display(int data)
{
  printf("%d ",data);fflush(stdout);
}

aluvalue() // calculate ALU value
{
  switch(alufunc){
    case 1:return(aluA+aluB);
    case 2:return(aluA-aluB);
    case 3:return(aluA*aluB);
    case 4:if (aluB != 0) return(aluA/aluB);
           return(0);
  }
  return(0);
}

compvalue() // calculate comparator value
{
  switch(compfunc){
    case 1:return((compA>compB)?1:0);
    case 2:return((compA==compB)?1:0);
  }
  return(0);
}
